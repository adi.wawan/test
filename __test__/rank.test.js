import { render, screen } from './test.utils';
import Ranks from '@pages/ranks';

describe('ranks page render', () => {
  it('should render the heading', () => {
    render(<Ranks users={[]} />);

    const heading = screen.getByText(/Leaderboard/i);

    // we can only use toBeInTheDocument because it was imported
    // in the jest.setup.js and configured in jest.config.js
    expect(heading).toBeInTheDocument();
  });
});
