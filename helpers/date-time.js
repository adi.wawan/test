const months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'Mei',
  'Jun',
  'Jul',
  'Aug',
  'Sept',
  'Okt',
  'Nov',
  'Des',
];

const days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];

export const dateFormat = (time) => {
  const d = new Date(time);
  const year = d.getFullYear();
  const date = d.getDate();

  const monthName = months[d.getMonth()];
  const dayName = days[d.getDay()];

  const hour = d.getHours();
  const minute = d.getMinutes();

  if (date.length < 2) date = '0' + date;
  // if (monthName.length < 2) monthName = '0' + monthName;
  if (hour.length < 2) hour = '0' + hour;
  if (minute.length < 2) minute = '0' + minute;

  return `${dayName}, ${date} ${monthName} ${year} ${
    String(hour).length < 2 ? '0' + hour : hour
  }:${String(minute).length < 2 ? '0' + minute : minute}`;
};
