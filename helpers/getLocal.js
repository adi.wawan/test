export const getUser = async () => {
  const user = await JSON.parse(localStorage.getItem('user'));
  return user;
};

export const getToken = async () => {
  const token = await JSON.parse(localStorage.getItem('token'));
  return token;
};
