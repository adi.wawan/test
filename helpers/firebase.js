import firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: 'AIzaSyDtVU6T71h6fUlSFj256HwdAjt__fpPBTo',
  authDomain: 'binar-e5dff.firebaseapp.com',
  projectId: 'binar-e5dff',
  storageBucket: 'binar-e5dff.appspot.com',
  messagingSenderId: '55924724235',
  appId: '1:55924724235:web:442c95aa8d74e549f0ed0e',
};

export const createUserProfile = async (userAuth, otherData) => {
  if (!userAuth) return;

  //ini akan nge-reference di firestore sesuai uid yg terdaftar
  const userRef = firestore.doc(`users/${userAuth.uid}`);
  const snapshot = userRef.get();

  if (!snapshot.exists) {
    const { email } = userAuth;
    const createdAt = new Date();

    try {
      await userRef.set({
        email,
        createdAt,
        ...otherData,
      });
    } catch (error) {
      console.log('error create user', error.message);
    }
  }
  return userRef;
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const firestore = firebase.firestore();
export const fireStorage = firebase.storage();

//   membuatt provider untuk memberikan akses, dgn new Google auth provider class dr authentication library
const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });

export const signinWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
