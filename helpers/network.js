import axios from 'axios';

export const callApi = async (payload) => {
  try {
    const request = await axios(payload);
    return request?.data;
  } catch (error) {
    return Promise.reject(error.response?.data);
  }
};
