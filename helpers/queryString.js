export const dashed = (name) => {
  return name.replace(/\s/g, '_');
};

export const unDashed = (name) => {
  return name.replace(/_/g, ' ');
};
