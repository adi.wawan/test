export const normalizeData = (data) => {
  return {
    ...data,
    users: data.users?.sort(
      (a, b) => b.userhistory.user_point - a.userhistory.user_point
    ),
  };
};

export const normalizeRanks = (data) => {
  return data.sort((a, b) => b.total_score - a.total_score);
};
