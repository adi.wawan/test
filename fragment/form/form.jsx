import React from 'react';

import { Container, Row, Card, Form } from 'react-bootstrap';
import FormInput from '../../components/form-input';
import CustomButton from '../../components/custom-button';
import TextLink from '../../components/link';
import { formList } from './form.utils';
import router from 'next/router';

const FormFragment = ({
  handleChange,
  handleSubmit,
  handleGoogle,
  type,
  error,
}) => {
  return (
    <>
      <Row>
        <Card className="card-sign mx-auto p-5" style={{ marginTop: '5rem' }}>
          <h2 className="text-center" style={{ textTransform: 'capitalize' }}>
            {type} Form
          </h2>
          <Form className="pt-3">
            {error && <p className="error">{error}</p>}

            {formList[type].map((item) => (
              <FormInput
                key={item.name}
                label={item.label}
                type={item.type}
                name={item.name}
                placeholder={item.placeholder}
                handleChange={(e) => handleChange(e)}
                required
              />
            ))}
            <CustomButton
              variant="primary"
              label={type === 'signin' ? 'Signin' : 'Signup'}
              onClick={handleSubmit}
            />
            {type === 'signin' && (
              <CustomButton
                variant="primary"
                isGoogle
                label="Signin with google"
                onClick={handleGoogle}
              />
            )}
            <Row className="ml-0">
              <p>Dont have an account ?&nbsp;</p>
              <TextLink
                label={type === 'signin' ? 'Signup' : 'Signin'}
                action={() =>
                  router.push(type === 'signin' ? '/signup' : '/signin')
                }
              />
            </Row>
            {type === 'signin' && (
              <TextLink
                label="forgot password ?"
                // action={() => router.push('/reset-password')}
                style={{ textAlign: 'center', marginTop: '-1rem' }}
              />
            )}
          </Form>
        </Card>
      </Row>
    </>
  );
};

export default FormFragment;
