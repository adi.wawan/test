export const formList = {
  signin: [
    { label: 'Email', type: 'email', name: 'email', placeholder: 'Email' },
    {
      label: 'Password',
      type: 'password',
      name: 'password',
      placeholder: 'Password',
    },
  ],
  signup: [
    {
      label: 'Username',
      type: 'text',
      name: 'username',
      placeholder: 'Username',
    },
    { label: 'Email', type: 'email', name: 'email', placeholder: 'Email' },
    {
      label: 'Password',
      type: 'password',
      name: 'password',
      placeholder: 'Password',
    },
  ],
};
