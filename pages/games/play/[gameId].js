import React from 'react';
import Image from 'next/image';
import { connect } from 'react-redux';
import {
  reauthenticate,
  checkServerSideCookie,
} from '@redux/actions/auth.action';
import { wrapper } from '@redux/store';
import GameRps from '@containers/rps/rps';
import GameLain from '@containers/other-game';
import { useSelector } from 'react-redux';

const GamePlay = ({ gameId }) => {
  const auth = useSelector((state) => state.auth);
  switch (gameId) {
    case '1':
      return (
        <>
          <GameRps gameId={gameId} />
        </>
      );
    default:
      return (
        <>
          <GameLain gameId={gameId} />
        </>
      );
  }
};

export const getServerSideProps = wrapper.getServerSideProps(async (ctx) => {
  const { gameId } = ctx.query;

  // checkServerSideCookie(ctx);

  // const state = ctx.store.getState();

  return {
    props: {
      gameId,
    },
  };
});

export default GamePlay;

// export default GamePlay;
