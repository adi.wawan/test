import React from 'react';
// import { useSelector } from 'react-redux';
import { wrapper } from '../../redux/store';
import GameContainer from '../../containers/game-list';
import axios from 'axios';

const Games = ({ gameList }) => {
  // const games = useSelector((state) => state.games);

  return (
    <>
      <GameContainer gameList={gameList} />
    </>
  );
};

export const getServerSideProps = wrapper.getServerSideProps(
  async ({ req, res, store }) => {
    // store.dispatch(getGameList());
    const state = store.getState();
    const result = await axios.get(`${process.env.API_HOST}/v2/games`);
    const gameList = await result.data.data;
    return {
      props: {
        gameList,
      },
    };
  }
);

export default Games;
