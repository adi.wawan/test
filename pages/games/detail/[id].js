import React, { useState } from 'react';
import Image from 'next/image';
import { wrapper } from '../../../redux/store';
import router from 'next/router';
import axios from 'axios';
import nullPic from '@assets/user_photo_null.png';
import { useSelector } from 'react-redux';
import { normalizeData } from '@helpers/normalize';
import Divider from '@components/divider';
import Button from '@components/custom-button';

const GameDetail = ({ gameDetail }) => {
  const [loading, setLoading] = useState(false);
  const { user } = useSelector((state) => state.auth);

  const check = (n, m) => {
    if (m.length > 0) {
      const filtered = m.map((x) => x.id);
      if (filtered.indexOf(n) !== -1) {
        return true;
      }
    }
    return false;
  };

  const dataDetail = normalizeData(gameDetail);

  const handleClick = () => {
    setLoading(true);
    const token = localStorage.getItem('token');
    if (!token) {
      return router.push(`/signin`);
    }
    router.push(`/games/play/${dataDetail.id}`);
  };

  // TODO: mapping sorting leaderboard
  return (
    <>
      <div className="row mt-3">
        <div className="col-md-3 mt-2 d-flex align-items-center justify-content-center flex-column">
          <div className="img-act-detail mr-3">
            <Image
              className="img-round rounded"
              src={dataDetail.thumbnail_url}
              width={200}
              height={200}
              alt="user-image"
            />
          </div>
          <Button type="sm" onClick={handleClick}>
            {check(user?.id, dataDetail.users) === true ? 'Play Again' : 'Play'}
          </Button>
        </div>
        <div className="col-md-9 mt-2">
          <div className="p-3 rounded">
            <h2 className="detail-title">{dataDetail.title}</h2>
            {/* <Divider bg="green" w={'55%'} h={5} marginBottom={'2rem'} /> */}
            {dataDetail.description}
          </div>
        </div>
      </div>
      <div className="divider bg-dark mt-4" />
      <div className="d-flex flex-column mt-3 mb-4">
        <h3>Ranks : </h3>
        <Divider bg="violet" w={'15%'} h={5} />

        {!dataDetail.users.length ? (
          <h2
            className="title text-center"
            style={{ width: 'auto', borderBottom: 0 }}
          >
            {' '}
            Never played, be the first one!{' '}
          </h2>
        ) : (
          dataDetail.users?.map((item, id) => (
            <div
              key={`${id}`}
              className="list-act align-items-center pt-1 pb-2 pl-3 rounded mt-2"
            >
              <div className="img-act mr-3">
                <Image
                  className="img-round rounded"
                  src={item.images ? item.images : nullPic}
                  width={200}
                  height={200}
                  alt="user-image"
                />
              </div>
              <div className="desc-act">
                <h2 className="game-name">{item.username}</h2>
                <p className="score-act">
                  Scores: {item.userhistory.user_point}
                </p>
              </div>
            </div>
          ))
        )}
      </div>
    </>
  );
};

export const getServerSideProps = wrapper.getServerSideProps(
  async ({ req, res, store, query }) => {
    const { id } = query;
    // const state = store.getState();
    // const gameDetail = state.games.filter((item) => +item.id === +id);

    // integrate to EP
    const result = await axios.get(`${process.env.API_HOST}/v2/games/${id}`);
    const gameDetail = await result.data.data;

    return {
      props: {
        gameDetail,
      },
    };
  }
);

export default GameDetail;
