import React, { useEffect, useState } from 'react';
import { FaFacebook, FaTwitter, FaEnvelope, FaLinkedin } from 'react-icons/fa';

const About = () => {
  const [client, setClient] = useState(false);

  useEffect(() => {
    if (process.browser) {
      setClient(true);
    }
  }, []);

  const member = [
    {
      id: 1,
      image: 'https://i.ibb.co/2t148dC/ikrom.jpg',
      name: 'Ikram Sobri',
      social_media: [
        {
          email: 'ikram.shabri@gmail.com',
          twitter: 'https://www.twitter.com',
          facebook: 'https://www.facebook.com/',
          linkedin: 'https://www.linkedin.com/in/ikram-shabri-647006169/',
        },
      ],
      role: ['Scrum Master', 'Web Developer'],
      desc:
        '“Jika kamu tidak menyerah, kamu masih memiliki kesempatan. Menyerah adalah kegagalan terbesar.” - Jack Ma',
    },
    {
      id: 2,
      image: 'https://i.ibb.co/sKz5wNF/adi-s.jpg',
      name: 'Adi Setyawan',
      social_media: [
        {
          email: 'adi.wawan90@gmail.com',
          twitter: 'https://www.twitter.com/@di_wawan90',
          facebook: 'https://www.facebook.com/adi.wawan.7',
          linkedin: 'https://www.linkedin.com/in/adi-setyawan-0b6a98131/',
        },
      ],
      role: ['Fullstack Developer'],
      desc:
        '“Jika Kamu tidak dapat menahan lelahnya belajar | Maka kamu harus sanggup menahan perihnya Kebodohan.” - Imam Syafii ',
    },
    {
      id: 3,
      image: 'https://i.ibb.co/6w42qV2/Sanca-Naufal.jpg',
      name: 'Naufal Sanca',
      social_media: [
        {
          email: 'sancanaufal@gmail.com',
          twitter: 'https://www.twitter.com/sancanaufal',
          facebook: 'https://www.facebook.com/',
          linkedin:
            'https://www.linkedin.com/in/naufal-sanca-lovandhika-52470664/',
        },
      ],
      role: ['BE Developer', 'UI Design'],
      desc:
        '“Jika saya gagal, saya tidak akan menyesalinya. Tetapi satu hal yang mungkin saya sesali adalah tidak mencobanya” – Jeff Bezos',
    },
    {
      id: 4,
      image: 'https://i.ibb.co/QNKT40M/risky.jpg',
      name: 'Risky Leonardo',
      social_media: [
        {
          email: 'riskyleonardo34@yahoo.com',
          twitter: 'https://www.twitter.com/@riskyyy_26',
          facebook: 'https://www.facebook.com/',
          linkedin: 'https://www.linkedin.com',
        },
      ],
      role: ['BE Developer', 'UI Design'],
      desc:
        '“Beberapa orang memimpikan kesuksesannya, sementara yang lainnya bangun setiap pagi untuk mewujudkan mimpinya” – Wayne Huizenga',
    },
    {
      id: 5,
      image: 'https://i.ibb.co/LCnpmGv/Kamandanu.jpg',
      name: 'Kamandanu Tresnawijaya',
      social_media: [
        {
          email: 'kamandanu.wijaya@gmail.com',
          twitter: 'https://www.twitter.com',
          facebook: 'https://www.facebook.com/',
          linkedin:
            'https://www.linkedin.com/in/kamandanu-tresnawijaya-07b69979/',
        },
      ],
      role: ['DevOps', 'Web Developer'],
      desc:
        '“Apa yang kita pikirkan menentukan apa yang akan terjadi pada kita. Jadi jika kita ingin mengubah hidup, kita perlu sedikit mengubah pikiran kita” – Wayne Dyer',
    },
  ];

  return (
    <div className="about-team pt-5">
      <h2
        className="title text-center"
        style={{ width: 'auto', borderBottom: 0 }}
      >
        About Our Team
      </h2>
      <div className="row justify-content-center">
        {member.map((item) => (
          <div class="col-md-4 mt-4">
            <div
              class="card profile-card-5"
              style={{ backgroundColor: '#57626d' }}
            >
              <div class="card-img-block">
                <img
                  class="card-img-top"
                  src={item.image}
                  alt="Card image cap"
                />
              </div>
              <div class="card-body pt-0">
                <h5 class="card-title text-center hvr-underline-from-center">
                  {item.name} <span>{item.role.join(', ')}</span>
                </h5>

                <p class="card-text text-center" style={{ color: '#e5f1f9' }}>
                  {item.desc}
                </p>
                <div className="icon-block d-flex flex-row justify-content-around">
                  {item.social_media.map((sos) => (
                    <>
                      <a href={sos.twitter} target="_blank" rel="noreferrer">
                        <FaTwitter size={20} color="#00aced" />
                      </a>
                      <a href={sos.facebook} target="_blank" rel="noreferrer">
                        <FaFacebook size={20} color="#3b5998" />
                      </a>
                      <a
                        href={`mailto:${sos.email}`}
                        target="_blank"
                        rel="noreferrer"
                      >
                        <FaEnvelope size={20} color="#4285F4" />
                      </a>
                      <a href={sos.linkedin} target="_blank" rel="noreferrer">
                        <FaLinkedin size={20} color="#007bb6" />
                      </a>
                    </>
                  ))}
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default About;
