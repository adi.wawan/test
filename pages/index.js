import { useSelector } from 'react-redux';
import axios from 'axios';

// import MyCarousel from '@components/carousel';
import { wrapper } from '../redux/store';
import GameList from '@containers/game-list';
import SlickCarousel from '@components/slide/carousel';

const Home = ({ gameList }) => {
  const auth = useSelector((state) => state.auth);

  return (
    <>
      {/* <MyCarousel gameList={gameList} />
      <Slick gameList={gameList} /> */}
      <SlickCarousel />
      <GameList gameList={gameList} />
    </>
  );
};

export const getServerSideProps = wrapper.getServerSideProps(
  async ({ req, res, store }) => {
    // dari redux
    // store.dispatch(getGameList());
    // const state = store.getState();

    // integrte to api
    const result = await axios.get(`${process.env.API_HOST}/v2/games`);
    const gameList = await result.data.data;
    return {
      props: {
        gameList,
      },
    };
  }
);

export default Home;
