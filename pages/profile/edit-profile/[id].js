import React, { useState, useEffect } from 'react';
import Image from 'next/image';
import nullPic from '@assets/user_photo_null.png';
import { HiPencilAlt } from 'react-icons/hi';
import { withRouter } from 'next/router';
import axios from 'axios';

import { useDispatch, useSelector } from 'react-redux';
import { LOADING, STOPLOADING, SET_USER } from '@redux/types';
import { toast } from 'react-toastify';

import { getCookie } from '@utils/cookies';
import CustomButton from '@components/custom-button';

const EditProfile = ({ router, userDetail }) => {
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.load);
  const [fileImg, setFileImg] = useState(null);
  const [isDisabled, setIsDisabled] = useState(true);
  const [userInfo, setUserInfo] = useState({
    ...userDetail,
  });

  function buildFileSelector() {
    const fileSelector = document.createElement('input');
    fileSelector.setAttribute('type', 'file');
    fileSelector.setAttribute('accept', 'image/*');
    fileSelector.onchange = function (e) {
      _handleImageChange(e);
    };
    return fileSelector;
  }

  const handleFileSelect = (e) => {
    e.preventDefault();
    buildFileSelector().click();
  };

  function _handleImageChange(e) {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      setFileImg({
        file: file,
        imagePreviewUrl: reader.result,
      });
    };
    reader.readAsDataURL(file);
  }

  const editHandle = (e) => {
    e.preventDefault();
    setIsDisabled(false);
  };

  const _handleSubmit = async (e) => {
    e.preventDefault();
    dispatch({ type: LOADING });
    // TODO: yg diupload -> fileImg.file

    let formData = new FormData();
    // formData.append('username', userInfo.username);
    // formData.append('email', userInfo.email);
    if (fileImg) {
      formData.append('image', fileImg.file);
      await axios
        .post(
          'https://api.imgbb.com/1/upload?key=bfea20be5f990347039af01337a564cf',
          formData
        )
        .then((res) => {
          setUserInfo({
            ...userInfo,
            images: res.data.data.display_url,
          });

          const dataUpdate = {
            username: userInfo.username,
            email: userInfo.email,
            images: res.data.data.display_url,
          };

          const config = {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `${getCookie('token')}`,
            },
          };

          axios
            .put(
              `${process.env.API_HOST}/users/${userDetail.id}`,
              dataUpdate,
              config
            )
            .then((res) => {
              localStorage.setItem('user', JSON.stringify(res.data.data));
              dispatch({ type: STOPLOADING });
              dispatch({ type: SET_USER, payload: res.data });
              toast.success('Success edit your profile..');
            })
            .catch((err) => {
              dispatch({ type: STOPLOADING });
              toast.error(err);
            });
        })
        .catch((err) => {
          toast.error(err);
        });
    } else {
      const dataUpdate = {
        username: userInfo.username,
        email: userInfo.email,
      };

      const config = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `${getCookie('token')}`,
        },
      };

      axios
        .put(
          `${process.env.API_HOST}/users/${userDetail.id}`,
          dataUpdate,
          config
        )
        .then((res) => {
          localStorage.setItem('user', JSON.stringify(res.data.data));
          dispatch({ type: STOPLOADING });
          dispatch({ type: SET_USER, payload: res.data });
          toast.success('Success edit your profile..');
        })
        .catch((err) => {
          dispatch({ type: STOPLOADING });
          toast.error(err);
        });
    }
  };

  const handleOnChange = (e) => {
    const { name, value } = e.target;
    setUserInfo({
      ...userInfo,
      [name]: value,
    });
  };

  return (
    <div className="Edit pt-5">
      <div className="upper-profile">
        <div className="img-wrapper rounded-circle">
          <Image
            className="rounded-circle"
            src={
              fileImg
                ? fileImg.imagePreviewUrl
                : userInfo.images
                ? userInfo.images
                : nullPic
            }
            width={200}
            height={200}
            alt="user-image"
          />
          <div className="icon-set-img ml-2" onClick={handleFileSelect}>
            <HiPencilAlt color={'gray'} size={30} />
          </div>
        </div>
        <form role="form" className="mt-3">
          <div className="form-group float-label-control d-flex">
            <input
              type="text"
              className="form-control"
              placeholder="Username"
              name="username"
              value={userInfo.username}
              onChange={(e) => handleOnChange(e)}
              disabled={isDisabled}
            />
          </div>
          <div className="form-group float-label-control  d-flex">
            <input
              type="email"
              className="form-control"
              placeholder="Email"
              name="email"
              value={userInfo.email}
              onChange={(e) => handleOnChange(e)}
              disabled={isDisabled}
            />
          </div>
          <div className="row d-flex justify-content-center">
            <CustomButton onClick={editHandle} type="edit">
              Edit
            </CustomButton>
            <button className="btn" onClick={_handleSubmit}>
              {isLoading ? 'Perocessing...' : 'Save'}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export const getServerSideProps = async ({ query }) => {
  const { id } = query;
  const res = await axios.get(`${process.env.API_HOST}/users/${id}`);
  const user = await res.data.data;

  return {
    props: {
      userDetail: user,
    },
  };
};

export default withRouter(EditProfile);
