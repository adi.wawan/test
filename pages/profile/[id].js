import React, { useState, useEffect } from 'react';
import Image from 'next/image';
import nullPic from '@assets/user_photo_null.png';
import * as IoIcons from 'react-icons/io5';
import { BiPrinter } from 'react-icons/bi';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { withRouter } from 'next/router';
import withAuth from '@helpers/withAuth';
import { wrapper } from '@redux/store';
import Divider from '@components/divider';
import Button from '@components/custom-button';
import Modal from '@components/modal';

import { callApi } from '@helpers/network';
import { dateFormat } from '@helpers/date-time';
import UserPayload from '@libraries/api/user';

const renderTooltip = (props) => (
  <Tooltip id="button-tooltip" {...props}>
    edit
  </Tooltip>
);

const Profile = ({ router, id, userDetail }) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className="profile pt-5">
      <div className="upper-profile">
        {/* <div className="d-flex justify-content-around flex-column absolute" style={{zIndex: 1, position: 'absolute'}}>
        </div> */}
        <div className="img-wrapper rounded-circle relative">
          <Divider
            bg="yellow"
            w={'72%'}
            h={10}
            position={'absolute'}
            left={'10%'}
            top={'4%'}
          />
          <Divider
            bg="blue"
            w={'130%'}
            h={10}
            position={'absolute'}
            left={'-20%'}
            top={'30%'}
          />
          <Divider
            bg="violet"
            w={'160%'}
            h={10}
            position={'absolute'}
            left={'-40%'}
            top={'55%'}
          />
          <Divider
            bg="green"
            w={'145%'}
            h={10}
            position={'absolute'}
            left={'-40%'}
            top={'80%'}
          />
          <Image
            className="rounded-circle absolute"
            src={userDetail.images ? userDetail.images : nullPic}
            width={200}
            height={200}
            alt="user-image"
          />
        </div>
        <div className="profile-username">{userDetail.username}</div>
        <Divider bg="yellow" w={'35%'} h={5} />
        <h5 className="profile-email">{userDetail.email}</h5>
        <h3>My Points: {userDetail.total_score || '0'}</h3>
        <OverlayTrigger
          placement="top"
          delay={{ show: 250, hide: 400 }}
          overlay={renderTooltip}
        >
          <div
            className="setting"
            onClick={() =>
              router.push(`/profile/edit-profile/${userDetail.id}`)
            }
          >
            <IoIcons.IoSettingsOutline size={35} />
          </div>
        </OverlayTrigger>
        <div
          className="setting"
          style={{ position: 'absolute', right: 0, top: 40 }}
          onClick={() => setIsOpen(!isOpen)}
        >
          <BiPrinter size={35} />
        </div>
      </div>
      <div className="divider bg-dark mt-2" />
      <div className="bottom-profile">
        <h3 className="mt-2">Recent Activity :</h3>
        <Divider bg="violet" w={'25%'} h={5} />

        <div className="activity-container">
          {!userDetail.games.length && (
            <h2 className="text-center">No Activity</h2>
          )}
          {userDetail.games?.map((item) => (
            <div
              key={item.id}
              className="list-act align-items-center p-1 pl-3 rounded mt-2 mb-2"
            >
              <div className="img-act mr-3">
                <Image
                  className="rounded"
                  src={item.thumbnail_url ? item.thumbnail_url : nullPic}
                  width={150}
                  height={150}
                  alt="user-image"
                />
              </div>
              <div className="desc-act" style={{ position: 'relative' }}>
                <h2 className="game-name-act">{item.name}</h2>
                <p className="time-act">{dateFormat(item.updatedAt)}</p>
                <div
                  className="btn-play"
                  onClick={() => router.push(`games/play/${item.id}`)}
                >
                  <Button
                    type="sm"
                    onClick={() => router.push(`games/play/${item.id}`)}
                  >
                    play again
                  </Button>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
      <Modal setIsOpen={setIsOpen} isOpen={isOpen} userDetail={userDetail} />
    </div>
  );
};

export const getServerSideProps = wrapper.getServerSideProps(
  async ({ query }) => {
    const { id } = query;
    // const payload = UserPayload.getDetail(id);
    // const result = await callApi(payload);
    const result = await axios.get(`${process.env.API_HOST}/users/${id}`);
    const userData = await result.data.data;

    return {
      props: {
        id,
        userDetail: userData,
      },
    };
  }
);

export default withRouter(Profile);
