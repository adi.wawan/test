// Whoami.js
import axios from 'axios';
import { connect } from 'react-redux';
import {
  reauthenticate,
  checkServerSideCookie,
} from '@redux/actions/auth.action';
import Router from 'next/router';

const Whoami = ({ user }) => (
  <div>
    {(user && (
      <div>
        <h2>Who am i</h2>
        {JSON.stringify(user)}
      </div>
    )) ||
      'Please sign in'}
  </div>
);

Whoami.getInitialProps = async (ctx) => {
  checkServerSideCookie(ctx);

  const { token } = ctx.store.getState().auth;

  if (token) {
    const response = await axios.get(`${process.env.API_HOST}/users/2`, {
      headers: {
        authorization: `${token}`,
        contentType: 'application/json',
      },
    });
    const user = response.data.data;
    return {
      user,
    };
  }
};

export default connect((state) => state, { reauthenticate })(Whoami);
