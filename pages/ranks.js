import React from 'react';
import { BsStarFill } from 'react-icons/bs';
import Image from 'next/image';
import NullPic from '../assets/user_photo_null.png';
import styled from '@emotion/styled';

import { callApi } from '@helpers/network';
import { normalizeRanks } from '@helpers/normalize';
import UserPayload from '@libraries/api/user';

const GamePlayedStyled = styled.div`
  background-color: cadetblue;
  padding: 3px 5px;
  border-radius: 10px;
  height: 60px;
  min-width: 60px;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-left: 5px;
`;

const ScoreStyled = styled.p`
  margin-bottom: 0;
  font-size: 12px;
  font-weight: 600;
`;

const NameStyled = styled.p`
  font-weight: 700;
  color: cyan;
  margin-bottom: 0;
`;

const Ranks = ({ users }) => {
  const usersList = normalizeRanks(users);
  return (
    <>
      <h2 className="title mt-3 pt-3">LeaderBoard </h2>
      {usersList.map((user, idx) => (
        <div className="d-flex flex-row align-items-center ">
          <p className="p-0 mb-0 mr-2">{`${idx + 1}.`}</p>

          <div
            key={user.id}
            className="d-flex flex-row justify-content-between mt-3 p-1 align-items-center rounded flex-grow-1"
            style={{ backgroundColor: '#6d7276' }}
          >
            <div className="d-flex flex-row align-items-center">
              <Image
                className="rounded"
                src={user.images ? user.images : NullPic}
                width={75}
                height={75}
                alt="user-image"
              />
              <div className="d-flex flex-column ml-2">
                <NameStyled>{user.username}</NameStyled>
                <div className="d-flex flex-row">
                  <BsStarFill color="gold" size={20} />
                  <p className="ml-1" style={{ marginBottom: 0 }}>
                    {user.total_score || '0'}
                  </p>
                </div>
              </div>
            </div>
            <div className="d-flex flex-row">
              {user.games?.map((game, idx) => (
                <GamePlayedStyled key={`${idx}`}>
                  <Image
                    className="rounded-circle"
                    src={game.thumbnail_url}
                    width={30}
                    height={30}
                    alt="game-image"
                  />
                  <ScoreStyled>{game.userhistory.user_point}</ScoreStyled>
                </GamePlayedStyled>
              ))}
            </div>
          </div>
        </div>
      ))}
    </>
  );
};

export const getServerSideProps = async () => {
  // const payload = UserPayload.getUsers();
  // const result = await callApi(payload);

  const result = await axios.get(`${process.env.API_HOST}/users`);
  const users = await result.data.data;

  return {
    props: {
      users: users,
    },
  };
};

export default Ranks;
