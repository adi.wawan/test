import React from 'react';
import SigninContainer from '@containers/signin';

const Signin = () => {
  return (
    <>
      <SigninContainer />
    </>
  );
};

export default Signin;
