import React from 'react';
// import App from 'next/app';
// import withRedux from 'next-redux-wrapper';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// import 'bootstrap/scss/bootstrap.scss';
import '../styles/game.scss';
import '../styles/styles.scss';
import '../styles/slides.css';
import '../styles/carousel.css';
import Head from 'next/head';

import { wrapper } from '../redux/store';
import { useStore, Provider, ReactReduxContext } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import MainLayout from '@components/layouts/main-layout';

const MyApp = ({ Component, pageProps }) => {
  const store = useStore((state) => state);

  return (
    <>
      <PersistGate persistor={store.__persistor} loading={null}>
        <Head>
          <title>Team1 - SSR</title>
        </Head>
        <MainLayout>
          <Component {...pageProps} />
          <ToastContainer autoClose={1000} />
        </MainLayout>
      </PersistGate>
    </>
  );
};

// MyApp.getInitialProps = async ({ Component, ctx }) => {
//   const pageProps = Component.getInitialProps
//     ? await Component.getInitialProps(ctx)
//     : {};
//   return { pageProps };
// };

export default wrapper.withRedux(MyApp);
