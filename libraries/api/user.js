const baseURL = process.env.API_HOST;

const config = {
  headers: {
    'Content-Type': 'application/json',
  },
};

function User() {
  const getUsers = () => ({
    baseURL,
    url: `/users`,
    method: 'GET',
    config,
  });

  const getDetail = (id) => ({
    baseURL,
    url: `/users/${id}`,
    method: 'GET',
    config,
  });

  return {
    getUsers,
    getDetail,
  };
}

export default User();
