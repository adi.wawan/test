import { getCookie } from '../../utils/cookies';
const baseURL = process.env.API_HOST;

const config = {
  headers: {
    'Content-Type': 'application/json',
  },
};

function Game() {
  const getAll = (data) => ({
    baseURL,
    url: '/v2/games',
    method: 'GET',
    config,
  });

  const gameDetail = (id) => ({
    baseURL,
    url: `/v2/games/${id}`,
    method: 'GET',
    config,
  });

  const updateScore = (gameId, data, conf) => ({
    baseURL,
    url: `v2/games/${gameId}`,
    method: 'PUT',
    data,
    conf,
  });

  return {
    getAll,
    gameDetail,
    updateScore,
  };
}

export default Game();
