// import { env } from '../../helpers/env';

const baseURL = process.env.API_HOST;

const config = {
  headers: {
    'Content-Type': 'application/json',
  },
};

function Auth() {
  const login = (data) => ({
    baseURL,
    url: '/users/login',
    method: 'POST',
    data,
    config,
  });

  const register = (data) => ({
    baseURL,
    url: '/users/register',
    method: 'POST',
    data,
    config,
  });

  return {
    login,
    register,
  };
}

export default Auth();
