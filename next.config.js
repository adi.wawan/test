// const withSass = require('@zeit/next-sass');
// const withCSS = require('@zeit/next-css');
const withImages = require('next-images');
const webpack = require('webpack');
// Initialize doteenv library
// require('dotenv').config();
// const Dotenv = require('dotenv-webpack');
const path = require('path');

module.exports = withImages({
  // future: {
  //   webpack5: true,
  // },
  cssLoaderOptions: {
    url: false,
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  domains: ['localhost:3001'],
  rules: [
    {
      loader: 'babel-loader',
      test: '/.(js$|jsx/)/',
      exclude: /node_modules/,
    },
    {
      test: /\.(jpg|jpeg|png|woff|woff2|eot|ttf|svg)$/,
      loader: 'url-loader?limit=100000',
    },
  ],
  images: {
    domains: ['i.ibb.co', 'localhost', 'still-savannah-78852.herokuapp.com'],
  },
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    // Add the new plugin to the existing webpack plugins
    // config.plugins.push(new Dotenv({ silent: true }));

    config.resolve.alias['@assets'] = path.join(__dirname, 'assets');
    config.resolve.alias['@components'] = path.join(__dirname, 'components');
    config.resolve.alias['@configs'] = path.join(__dirname, 'configs');
    config.resolve.alias['@containers'] = path.join(__dirname, 'containers');
    config.resolve.alias['@dummies'] = path.join(__dirname, 'dummies');
    config.resolve.alias['@helpers'] = path.join(__dirname, 'helpers');
    config.resolve.alias['@redux'] = path.join(__dirname, 'redux');
    config.resolve.alias['@interfaces'] = path.join(__dirname, 'interfaces');
    config.resolve.alias['@libraries'] = path.join(__dirname, 'libraries');
    config.resolve.alias['@pages'] = path.join(__dirname, 'pages');
    config.resolve.alias['@utils'] = path.join(__dirname, 'utils');
    config.resolve.alias['@service-worker'] = path.join(
      __dirname,
      'service-worker'
    );

    const env = Object.keys(process.env).reduce((acc, curr) => {
      acc[`process.env.${curr}`] = JSON.stringify(process.env[curr]);
      return acc;
    }, {});

    config.plugins.push(new webpack.DefinePlugin(env));

    return config;
  },
});

// const withSass = require('@zeit/next-sass');
// module.exports = withSass({
// /* bydefault config  option Read For More Optios
// here https://github.com/vercel/next-plugins/tree/master/packages/next-sass*/
// cssModules: true
// })
// module.exports = {
// /* Add Your Scss File Folder Path Here */
// sassOptions: {
// includePaths: [path.join(__dirname, 'styles')],
// },
// }
