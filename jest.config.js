// jest.config.js
module.exports = {
  setupFilesAfterEnv: ['./jest.setup.js'],
  moduleNameMapper: {
    '^@components(.*)$': '<rootDir>/components$1',
    '^@pages(.*)$': '<rootDir>/pages$1',
    '^@hooks(.*)$': '<rootDir>/hooks$1',
    '^@assets(.*)$': '<rootDir>/assets$1',
    '^@helpers(.*)$': '<rootDir>/helpers$1',
    '^@libraries(.*)$': '<rootDir>/libraries$1',
    '^@styles(.*)$': '<rootDir>/styles$1',
    '^@styles(.*)$': '<rootDir>/styles$1',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__test__/fileTransformer.js',
  },
};
