import Router from 'next/router';
import { SET_USER } from '../types';

import { callApi } from '../../helpers/network';
import UserPayload from '../../libraries/api/user';

export const setUser = (data) => async (dispatch) => {
  try {
    const payload = UserPayload.getDetail(data);
    const result = await callApi(payload);
    if (result) {
      localStorage.setItem('user', JSON.stringify(result.data));
      dispatch({ type: SET_USER, payload: result });
    }
  } catch (error) {
    console.log(error);
  }
};
