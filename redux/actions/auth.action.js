import Router from 'next/router';
import { AUTHENTICATE, UNAUTHENTICATE, LOADING, STOPLOADING } from '../types';
import { toast } from 'react-toastify';

import { callApi } from '../../helpers/network';
import AuthPayload from '../../libraries/api/auth';
import { removeCookie, setCookie, getCookie } from '../../utils/cookies';

export const authenticate = (user) => async (dispatch) => {
  try {
    dispatch({ type: LOADING });
    const payload = AuthPayload.login(user);
    const result = await callApi(payload);
    if (result) {
      setCookie('token', result.token);
      localStorage.setItem('user', JSON.stringify(result.data));
      localStorage.setItem('token', JSON.stringify(result.token));
      Router.push('/');
      dispatch({ type: AUTHENTICATE, payload: result.token });
      dispatch({ type: STOPLOADING });
      toast.success('Login Success!');
    }
  } catch (error) {
    toast.error(error.message);
    dispatch({ type: STOPLOADING });
  }
};

export const register = (user) => async (dispatch) => {
  try {
    dispatch({ type: LOADING });
    const payload = AuthPayload.register(user);
    const result = await callApi(payload);
    if (result) {
      setCookie('token', result.token);
      localStorage.setItem('user', JSON.stringify(result.data));
      localStorage.setItem('token', JSON.stringify(result.token));
      Router.push('/');
      dispatch({ type: AUTHENTICATE, payload: result });
      dispatch({ type: STOPLOADING });
      toast.success('Register Success!');
    }
  } catch (error) {
    toast.error(error.message);
    dispatch({ type: STOPLOADING });
  }
};

export const reauthenticate = (token) => {
  return (dispatch) => {
    dispatch({ type: AUTHENTICATE, payload: { token } });
  };
};

export const unAuthenticate = () => {
  return (dispatch) => {
    removeCookie('token');
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    Router.replace('/');
    dispatch({ type: UNAUTHENTICATE });
  };
};

// check if the page is being loaded on the server, and if so, get auth token from the cookie
export const checkServerSideCookie = (ctx) => {
  if (ctx.isServer) {
    if (ctx.req.headers.cookie) {
      const token = getCookie('token', ctx.req);
      ctx.store.dispatch(reauthenticate(token));
    }
  } else {
    const token = ctx.store.getState().auth.token;

    if (token && (ctx.pathname === '/signin' || ctx.pathname === '/signup')) {
      setTimeout(function () {
        Router.push('/');
      }, 0);
    }
  }
};
