import { CAT_ADV, CAT_STR } from '../types';

const initCat = {
  category: 'all',
};

const catReducer = (state = initCat, action) => {
  switch (action.type) {
    case CAT_ADV:
      return {
        ...state,
        category: 'adventure',
      };
    case CAT_STR:
      return {
        ...state,
        category: 'stategy', // it must be strtegy not stategy ! typo on db!
      };
    case 'ALL':
      return {
        ...state,
        category: 'all',
      };
    default:
      return {
        ...state,
        category: 'all',
      };
  }
};

export default catReducer;
