import { AUTHENTICATE, UNAUTHENTICATE, SET_USER } from '../types';

const initialState = {
  token: '',
  isLoggedin: false,
  user: {},
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'persist/REHYDRATE': {
      const data = action.payload;
      if (data) {
        return {
          ...state,
          ...data.auth,
        };
      }
    }
    case AUTHENTICATE:
      return {
        ...state,
        token: action.payload.token,
        isLoggedin: true,
      };
    case UNAUTHENTICATE:
      localStorage.removeItem('user');
      localStorage.removeItem('token');
      return {
        ...state,
        token: '',
        isLoggedin: false,
        user: {},
      };
    case SET_USER:
      return {
        ...state,
        user: action.payload.data,
      };
    default:
      return {
        ...state,
      };
  }
};

export default authReducer;
