import { GAMELIST } from '../types';
const gameList = [
  {
    id: 1,
    title: 'Rock Paper Scissor',
    thumbnail_url: 'https://i.ibb.co/zmZGyZD/rps-icon.png',
    gameUrl: 'rps',
    playCount: 3,
    status: 'available',
    description: 'Permainan lama beradu dengan tangan',
    category: ['stategy'],
  },
  {
    id: 2,
    title: 'Guest Word',
    thumbnail_url: 'https://i.ibb.co/0Q4hHMs/tebak-kata.png',
    gameUrl: 'guest-word',
    playCount: 3,
    status: 'unavailable',
    description: 'lorem ipsum dolor',
    category: ['stategy', 'adventure'],
  },
  {
    id: 3,
    title: 'Guest Color',
    thumbnail_url: 'https://i.ibb.co/3fnk14Z/tebak-warnapng.png',
    gameUrl: 'guest-color',
    playCount: 3,
    status: 'unavailable',
    description: 'lorem ipsum dolor',
    category: ['stategy', 'adventure'],
  },
  {
    id: 4,
    title: 'Tic-Tac-Toe',
    thumbnail_url: 'https://i.ibb.co/xHxN3Nj/tictactoe.jpg',
    gameUrl: 'tic-tac-toe',
    playCount: 3,
    status: 'unavailable',
    description: 'lorem ipsum dolor',
    category: ['stategy'],
  },
];

const gameReducer = (state = gameList, action) => {
  switch (action.type) {
    case GAMELIST:
      return state;
    default:
      return state;
  }
};

export default gameReducer;
