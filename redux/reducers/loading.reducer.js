import { LOADING, STOPLOADING } from '../types';

const initLoad = {
  isLoading: false,
};

const loadReducer = (state = initLoad, action) => {
  switch (action.type) {
    case LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case STOPLOADING:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return {
        ...state,
      };
  }
};

export default loadReducer;
