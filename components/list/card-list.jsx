/* eslint-disable react/prop-types */
import React from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import { FaStar } from 'react-icons/fa';
import './list.styles.scss';

const CardList = ({ label, title, type, point, score, date }) => {
  if (type === 'histories') {
    return (
      <div className="content-wrapper">
        <Row>
          <Col>
            <Card.Title>{title}</Card.Title>
          </Col>
          <Col>
            <Card.Text style={{ textAlign: 'end', alignItems: 'center' }}>
              <FaStar color="gold" /> {point}
            </Card.Text>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card.Text>{date}</Card.Text>
          </Col>
          <Col>
            <Card.Text style={{ textAlign: 'end' }}>Score: {score}</Card.Text>
          </Col>
        </Row>
      </div>
    );
  }

  return (
    <div className="content-wrapper">
      <Card.Text
        style={{ marginBottom: '0.125rem', color: 'rgba(26,26,26,.5)' }}
      >
        {label}
      </Card.Text>
      <Card.Title>{title}</Card.Title>
    </div>
  );
};

export default CardList;
