/* eslint-disable react/prop-types */
import React from 'react';

const FormInput = ({ handleChange, label, ...otherProps }) => {
  return (
    <div className="form-group">
      {/* <label className='form-label'>{label}</label> */}
      <input className="form-control" onChange={handleChange} {...otherProps} />
    </div>
  );
};

export default FormInput;
