import React from 'react';
import { withRouter } from 'react-router-dom';
import { Container, Card } from 'react-bootstrap';
import { useGameCtx } from '../../contex/game/gameContext';

const GameList = ({ match, history }) => {
  const { gameList } = useGameCtx();

  return (
    <Container>
      <div className="card-deck">
        {gameList.map((item) => (
          <Card
            mb={4}
            className={`box ${item.status === 'available' ? 'available' : ''}`}
            key={item.id}
            onClick={() => history.push(`${match.url}/detail/${item.gameUrl}`)}
          >
            <img
              className="card-img-top img-fluid"
              src={item.thumbnail_url}
              alt={item.name}
            />
            <div className="card-body">
              <h4 className="card-title">{item.title}</h4>
              <p className="card-text">{item.description}</p>
              <p className="card-text">
                <small className="text-muted">Last updated 3 mins ago</small>
              </p>
            </div>
          </Card>
        ))}
      </div>
    </Container>
  );
};

export default withRouter(GameList);
