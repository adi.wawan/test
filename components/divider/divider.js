import React from 'react';

const Divider = ({ bg, w = '100%', h = 2, ...rest }) => {
  const bckg = (bg) => {
    switch (bg) {
      case 'violet':
        return 'linear-gradient(90deg, rgba(84,53,105,1) 0%, rgba(141,40,87,1) 100%)';
      case 'blue':
        return 'linear-gradient(90deg, rgba(47,102,156,1) 0%, rgba(62,197,206,1) 100%)';
      case 'yellow':
        return 'linear-gradient(90deg, rgba(52,62,87,1) 0%, rgba(254,234,0,1) 100%)';
      case 'green':
        return 'linear-gradient(90deg, rgba(41,86,87,1) 0%, rgba(2,222,184,1) 100%)';
      default:
        break;
    }
  };

  return (
    <div
      style={{
        background: bckg(bg),
        width: w,
        height: h,
        borderRadius: h / 2,
        ...rest,
      }}
    />
  );
};

export default Divider;
