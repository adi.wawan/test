import React from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import { BiDownload } from 'react-icons/bi';
import MyPDF from '../pdf-viewer';
import Pdf from '../pdf-viewer/pdf';
import { PDFDownloadLink } from '@react-pdf/renderer';

const MyModal = ({ isOpen, setIsOpen, userDetail }) => {
  return (
    <>
      <div
        className="d-flex align-items-center justify-content-center"
        style={{ height: '50vh', width: '50vw' }}
      ></div>

      <Modal show={isOpen} onHide={setIsOpen} backdrop="static">
        <Modal.Header closeButton>
          <div className="download" style={{ cursor: 'pointer' }}>
            <PDFDownloadLink
              document={<Pdf userDetail={userDetail} />}
              fileName="my_detail_game.pdf"
            >
              {({ blob, url, loading, error }) =>
                loading ? '...' : <BiDownload color="#1f2942" size={30} />
              }
            </PDFDownloadLink>
          </div>
        </Modal.Header>
        <Modal.Body>
          <MyPDF userDetail={userDetail} />
        </Modal.Body>
      </Modal>
    </>
  );
};

export default MyModal;
