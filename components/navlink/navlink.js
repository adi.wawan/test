import React from 'react';
import Link from 'next/link';
import { useRouter, withRouter } from 'next/router';

const NavLink = ({ children, href, as, activeClassName, ...rest }) => {
  const child = React.Children.only(children);
  const router = useRouter();
  const { asPath } = useRouter();
  const childClassName = child.props.className || '';

  // pages/index.js will be matched via props.href
  // pages/about.js will be matched via props.href
  // pages/[slug].js will be matched via props.as

  const className =
    asPath === href || asPath === as
      ? `${childClassName} ${activeClassName}`.trim()
      : childClassName;

  return (
    <Link href={href} as={as} {...rest}>
      {React.cloneElement(child, {
        className: className || null,
      })}
    </Link>
  );
  // return (
  //   <Link href={href}>
  //     {React.cloneElement(child, {
  //       "aria-current": router.pathname === href ? "page" : null
  //     })}
  //   </Link>
  // );
};

export default withRouter(NavLink);
