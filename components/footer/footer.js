import React from 'react';
import styled from '@emotion/styled';

export const FooterStyle = styled.div`
  margin-top: 3rem;
  display: flex;
  justify-content: flex-end;
  color: cadetblue;
  font-size: 14px;
`;

export const FooterText = styled.p`
  ${'' /* position: fixed; */}
  bottom: 0;
`;

const Footer = () => {
  return (
    <FooterStyle>
      <FooterText>Copyright Team1 FSW-4 Binar 2021</FooterText>
    </FooterStyle>
  );
};

export default Footer;
