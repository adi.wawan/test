/* eslint-disable react/prop-types */
import React from 'react';
import './custom-button.module.scss';
import styled from '@emotion/styled';
import { useSelector } from 'react-redux';

export const BtnStyle = styled.button`
  background-color: #353637;
  color: #fff;
  padding: 1rem;
  text-decoration: none;
  width: 60px;
  height: 60px;
  border-radius: 5px;
  border-style: none;
`;

export const BtnStyleSM = styled.button`
  background-color: #11467c;
  color: #fff;
  text-decoration: none;
  width: 120px;
  height: 30px;
  border-radius: 20px;
  border-style: none;
  box-shadow: 4px 4px 15px 0px rgba(0,0,0,0.55);
  -webkit-box-shadow: 4px 4px 15px 0px rgba(0,0,0,0.55);
  -moz-box-shadow: 4px 4px 15px 0px rgba(0,0,0,0.55);
  :hover {
    background-color: #073767;
    color: #f7b048;
  }
`;

const CustomButton = ({
  label,
  children,
  isGoogle,
  variant,
  type,
  ...otherProps
}) => {
  const {isLoading} = useSelector((state) => state.load); // this is hook for accessing global state from redux

  if (type === 'edit') {
    return (
      <BtnStyle {...otherProps}>
      {children}
      </BtnStyle>
    )
  }

  if (type === 'sm') {
    return (
      <BtnStyleSM {...otherProps}>
      {children}
      </BtnStyleSM>
    )
  }
  return (
    <button
      type="button"
      className={`${isGoogle ? 'isgoogle' : 'btn '} btn-${variant}`}
      {...otherProps}
    >
      {isLoading && !isGoogle ? 'Processing' : label}
    </button>
  );
};

export default CustomButton;
