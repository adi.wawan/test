import React from 'react';
import { Container } from 'react-bootstrap';
import MyNavbar from '../../navbar';
import Footer from '../../footer';

const Layout = ({ children }) => {
  return (
    <>
      <MyNavbar />
      <Container className="container-fluid mt-5">
        <main>{children}</main>
        <Footer />
      </Container>
    </>
  );
};

export default Layout;
