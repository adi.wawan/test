import React from 'react';
import { withRouter } from 'react-router-dom';
import { Container, Row } from 'react-bootstrap';
import TextLink from '../link/text-link';

const NotFound = ({ history }) => {
  return (
    <Container>
      <Row className="mt-5">
        <div
          className="mx-auto mt-5"
          style={{ alignItems: 'center', justifyContent: 'center' }}
        >
          <h1>Oops,...</h1>
          <div>Sorry, Nothing to see here</div>
          <p>
            Maybe the page that you looking for was deleted, <br />
            or has been under maintaining
          </p>
          <TextLink label="Go to Homepage" action={() => history.push('/')} />
        </div>
      </Row>
    </Container>
  );
};

export default withRouter(NotFound);
