import React, { useState } from 'react';

import { Container, Row, Card, Form } from 'react-bootstrap';
import FormInput from '../form-input';
import CustomButton from '../custom-button';
import { withRouter } from 'react-router-dom';
import TextLink from '../link/text-link';
import { auth } from '../../helpers/firebase';
import { toast } from 'react-toastify';

const ResetPassword = ({ history }) => {
  const [email, setEmail] = useState('');
  const [emailHasBeenSent, setEmailHasBeenSent] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    auth
      .sendPasswordResetEmail(email)
      .then(() => {
        setEmailHasBeenSent(true);
        setTimeout(() => {
          setEmailHasBeenSent(false);
        }, 3000);
        toast.success('link has been sent, please check your email');
      })
      .catch(() => {
        toast.warn(
          'Error, maybe your email unregistered or not a correct google email addres!'
        );
      });
  };
  return (
    <Container>
      <Row>
        <Card className="card-sign mx-auto p-5" style={{ marginTop: '5rem' }}>
          <h2 className="text-center" style={{ textTransform: 'capitalize' }}>
            Reset Form
          </h2>
          <Form className="pt-3">
            <FormInput
              label="Email addres"
              type="email"
              name="email"
              placeholder="Your registered email"
              handleChange={(e) => setEmail(e.target.value)}
              required
            />
            <CustomButton
              variant="primary"
              label="Send email verification"
              onClick={handleSubmit}
            />

            <TextLink
              label="Cancel"
              action={() => history.push('/signin')}
              style={{ textAlign: 'center' }}
            />
          </Form>
        </Card>
      </Row>
    </Container>
  );
};

export default withRouter(ResetPassword);
