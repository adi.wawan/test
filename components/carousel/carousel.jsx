import React from 'react';
import { Carousel } from 'react-bootstrap';
import Image from 'next/image';

const MyCarousel = ({ gameList }) => {
  return (
    <>
      <Carousel interval={5000} style={{ height: '450px', marginTop: '5rem' }}>
        {gameList.map((item) => (
          <Carousel.Item key={item.id}>
            <Image
              className="d-block"
              src={item.thumbnail_url}
              alt="img-slide"
              style={{ margin: 'auto', height: '450px' }}
              height={450}
              width={450}
              // layout="responsive"
            />
          </Carousel.Item>
        ))}
      </Carousel>
    </>
  );
};

export default MyCarousel;
