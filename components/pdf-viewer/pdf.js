import React from 'react';
import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  Image,
  Link,
} from '@react-pdf/renderer';
import nullPic from '@assets/user_photo_null.png';
import { dateFormat } from '@helpers/date-time';

// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: 'column',
  },
  image: {
    width: '10%',
    padding: 5,
  },
  centerImage: {
    marginTop: 20,
    paddingTop: 10,
    alignItems: 'center',
    display: 'flex',
    alignItems: 'center',
  },
  text: {
    width: '100%',
    backgroundColor: '#fff',
    paddingHorizontal: 50,
    paddingVertical: 10,
    color: 'blue',
    fontWeight: 'bold',
    fontSize: '20px',
  },
  detailAct: {
    display: 'flex',
    flexDirection: 'row',
    marginHorizontal: 50,
    marginBottom: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#1f2942',
  },
  act: {
    width: '100%',
    backgroundColor: '#fff',
    paddingHorizontal: 50,
    paddingVertical: 5,
    color: '#212121',
  },
});

const MyDocument = ({ userDetail }) => {
  return (
    <Document>
      <Page style={styles.page} size="A4">
        <View style={styles.centerImage}>
          <Image
            style={styles.image}
            src={userDetail.images ? userDetail.images : nullPic}
          />
          <Text
            style={{
              ...styles.text,
              textAlign: 'center',
              paddingVertical: 0,
              color: '#1f2942',
            }}
          >
            {userDetail.username}
          </Text>
          <Text
            style={{
              ...styles.text,
              textAlign: 'center',
              paddingVertical: 0,
              // textDecoration: 'underline',
              color: '#2c5b75',
            }}
          >
            {userDetail.email}
          </Text>
          <Text
            style={{
              ...styles.text,
              textAlign: 'center',
              color: '#1f2942',
            }}
          >
            Points: {userDetail.total_score || '0'}
          </Text>
        </View>
        <Text
          style={{
            ...styles.text,
            paddingVertical: 10,
            textDecoration: 'underline',
            color: '#2c5b75',
          }}
        >
          Activity:
        </Text>
        {userDetail.games?.map((item) => (
          <View key={item.id} style={styles.detailAct}>
            <Image
              style={{ ...styles.image, height: 70, width: 70 }}
              src={item.thumbnail_url ? item.thumbnail_url : nullPic}
            />
            <View style={{ display: 'flex', flexDirection: 'column' }}>
              <Text style={styles.act}>{item.name}</Text>
              <Text style={styles.act}>{dateFormat(item.updatedAt)}</Text>
            </View>
          </View>
        ))}
        <Text style={styles.act}>
          See n Play:{' '}
          <Link style={styles.text} src="https://team1-game.herokuapp.com/">
            team1-game.herokuapp.com
          </Link>
        </Text>
      </Page>
    </Document>
  );
};

export default MyDocument;
