import React, { useState, useEffect } from 'react';
import { PDFViewer } from '@react-pdf/renderer';
import Pdf from './pdf';

const DocViewer = ({ userDetail }) => {
  const [client, setClient] = useState(false);

  useEffect(() => {
    if (process.browser) {
      setClient(true);
    }
  }, []);

  return (
    client && (
      <PDFViewer width="100%" height="900px">
        <Pdf userDetail={userDetail} />
      </PDFViewer>
    )
  );
};

export default DocViewer;
