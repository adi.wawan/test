/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
// import './text-link.module.scss';

const TextLink = ({ label, action, ...rest }) => {
  return (
    <p className="text-link" onClick={action} {...rest}>
      {label}
    </p>
  );
};

export default TextLink;
