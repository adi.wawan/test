import styled from '@emotion/styled';

export const NavStyle = styled.nav`
  display: flex;
  justify-content: space-between;

  a {
    background-color: #353637;
    color: #fff;
    padding: 1rem;
    text-decoration: none;

    &[aria-current] {
      background-color: #faf9f4;
      color: #353637;
    }
  }
`;
