import { useState, useEffect } from 'react';
import Head from 'next/head';
import router from 'next/router';
import { useSelector, useDispatch, connect } from 'react-redux';
import * as IoIcons from 'react-icons/io';

import {
  Container,
  Navbar,
  NavDropdown,
  Nav,
  Form,
  FormControl,
} from 'react-bootstrap';
import Image from 'next/image';
import { unAuthenticate } from '../../redux/actions/auth.action';
import styles from './navbar.module.scss';
import NavLink from '../navlink';
import { CAT_ADV, CAT_STR } from '@redux/types';

const MyNavbar = ({ unAuthenticate }) => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const [showScroll, setShowScroll] = useState(false);
  const [isDashboard, setIsDasboard] = useState(false);
  const [category, setCategory] = useState('all');

  useEffect(() => {
    if (auth.isLoggedin) {
      setIsDasboard(true);
    } else {
      setIsDasboard(false);
    }
  }, []);

  const checkScrollTop = () => {
    if (!showScroll && window.pageYOffset >= 100) {
      setShowScroll(true);
    } else if (showScroll && window.pageYOffset <= 100) {
      setShowScroll(false);
    }
  };

  const scrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  useEffect(() => {
    window.addEventListener('scroll', checkScrollTop);
    return () => {
      window.removeEventListener('scroll', checkScrollTop);
    };
  });

  const handleCategory = (e) => {
    // setCategory(e.target.value);
    dispatch({ type: e });
  };

  return (
    <>
      <Navbar
        className="navbar-bg-dark fixed-top"
        expand="lg"
        style={showScroll ? { backgroundColor: 'rgba(0,0,0,0.5)' } : null}
      >
        <Container className="container">
          <Navbar.Brand className="logo" onClick={() => router.push('/')}>
            Team 1
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <NavDropdown
                title="Category"
                id="nav-dropdown"
                onSelect={handleCategory}
              >
                <NavDropdown.Item eventKey="CAT_ALL">All</NavDropdown.Item>
                <NavDropdown.Item eventKey="CAT_ADV">
                  Adventure
                </NavDropdown.Item>
                <NavDropdown.Item eventKey="CAT_STR">Strategy</NavDropdown.Item>
              </NavDropdown>
              {/* <NavLink activeClassName="active" href="/whoami">
                <a className={`nav-link ${styles.navlink}`}>Contact Us</a>
              </NavLink> */}
              <NavLink activeClassName="active" href="/about">
                <a className={`nav-link ${styles.navlink}`}>About</a>
              </NavLink>
              <NavLink activeClassName="active" href="/ranks">
                <a className={`nav-link ${styles.navlink}`}>Ranks</a>
              </NavLink>
              {!auth.isLoggedin && (
                <>
                  <NavLink activeClassName="active" href="/signin">
                    <a className={`nav-link ${styles.navlink}`}>Signin</a>
                  </NavLink>
                  <NavLink activeClassName="active" href="/signup">
                    <a className={`nav-link ${styles.navlink}`}>Signup</a>
                  </NavLink>
                </>
              )}
            </Nav>

            <Nav>
              {/* <Form inline className="has-search">
                <span className="fa fa-search form-control-feedback">
                  <IoIcons.IoMdSearch color="#c4c4c4" size={28} />
                </span>
                <FormControl
                  type="text"
                  placeholder="Search"
                  className="mr-sm-2"
                />
              </Form> */}
              {auth.isLoggedin && (
                <div className={'row align-items-center'}>
                  <NavLink href={`/profile/${auth.user.id}`}>
                    <a className={`nav-link`}>
                      {auth.user.images ? (
                        <Image
                          className="rounded-circle"
                          src={auth?.user?.images}
                          width={28}
                          height={28}
                          alt="user-image"
                        />
                      ) : (
                        <IoIcons.IoMdContact color="#c4c4c4" size={28} />
                      )}
                    </a>
                  </NavLink>
                  <NavLink
                    activeClassName="active"
                    href={`/profile/${auth.user?.id}`}
                  >
                    <a className={`nav-link ${styles.navlink}`}>My Profile</a>
                  </NavLink>
                  <Nav activeClassName="active" href="/">
                    <a
                      className={`nav-link ${styles.navlink}`}
                      onClick={unAuthenticate}
                    >
                      Logout
                    </a>
                  </Nav>
                </div>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, { unAuthenticate })(MyNavbar);
