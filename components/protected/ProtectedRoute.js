import * as React from 'react';
import { Route, Redirect } from 'react-router-dom';
// import { useUserCtx } from '../../contex/userContex';
import { useAuthCtx } from '../../contex/auth/authContex';

const ProtectedRoute = ({ component: Component, ...rest }) => {
  // const { isAuth } = useUserCtx();
  const [states] = useAuthCtx();

  return (
    <Route
      {...rest}
      render={(props) => {
        return states.isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/signin',
              state: { from: props.location },
            }}
          />
        );
      }}
    />
  );
};

export default ProtectedRoute;
