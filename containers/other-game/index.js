import React, { useState, useEffect } from 'react';
import Image from 'next/image';
import { wrapper } from '@redux/store';
import router from 'next/router';
import axios from 'axios';
import nullPic from '@assets/user_photo_null.png';
import { toast } from 'react-toastify';
import { getCookie } from '@utils/cookies';

const GameLain = ({ gameId }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [gameDetail, setGameDetail] = useState([]);
  const [clicked, setClicked] = useState(false);
  const [score, setScore] = useState({
    pScore: 0,
    cScore: 0,
  });

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (!token) {
      return router.push(`/signin`);
    }
  }, []);

  const getDataDetail = async () => {
    const result = await axios.get(
      `${process.env.API_HOST}/v2/games/${gameId}`
    );
    setGameDetail(result.data.data);
  };

  useEffect(() => {
    getDataDetail();
  }, [gameId]);

  const handleClick = () => {
    setClicked(true);
    setScore({
      pScore: Math.floor(3 * Math.random()),
      cScore: Math.floor(3 * Math.random()),
    });
  };

  // FOR UPDATE SCORE

  const point = (p, c) => {
    if (p > c) {
      return 3;
    } else if (p < c) {
      return 0;
    } else {
      return 1;
    }
  };

  const resultStatus = (p, c) => {
    if (p > c) {
      return 'win';
    } else if (p < c) {
      return 'lose';
    } else {
      return 'draw';
    }
  };

  const configToken = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `${getCookie('token')}`,
    },
  };

  // store data ke BE
  const dataScores = {
    game_score: `${score.pScore} : ${score.cScore}`,
    user_point: point(score.pScore, score.cScore),
    status: resultStatus(score.pScore, score.cScore),
  };

  const handleSubmit = async (e) => {
    setIsLoading(true);
    await axios
      .put(
        `${process.env.API_HOST}/v2/games/${gameId}`,
        dataScores,
        configToken
      )
      .then((res) => {
        setIsLoading(false);
        toast.success('Success..');
        setClicked(false);
      })
      .catch((err) => {
        setIsLoading(false);
        toast.error(err);
      });
  };

  return (
    <>
      <div className="row mt-3">
        <div className="col-md-3 mt-2 d-flex align-items-center justify-content-center flex-column mx-auto">
          <div className="img-act-detail mx-auto">
            <Image
              className="img-round rounded"
              src={
                gameDetail.thumbnail_url ? gameDetail.thumbnail_url : nullPic
              }
              width={200}
              height={200}
              alt="user-image"
            />
          </div>
          <h5>{gameDetail.title}</h5>
          <button className="btn" onClick={handleClick}>
            Play
          </button>
        </div>
      </div>
      <div className="divider bg-dark mt-4" />
      <div className="d-flex justify-content-center mt-3">
        <button
          className="btn bg-primary"
          onClick={handleSubmit}
          disabled={!clicked}
        >
          {isLoading ? 'Processing..' : 'Save'}
        </button>
      </div>
    </>
  );
};

// export const getServerSideProps = wrapper.getServerSideProps(
//   async ({ req, res, store, query }) => {
//     // const { id } = query;
//     // const state = store.getState();
//     // const gameDetail = state.games.filter((item) => +item.id === +id);

//     // integrate to EP
//     const result = await axios.get(`${process.env.API_HOST}/v2/games/${gameId}`);
//     const gameDetail = await result.data.data;

//     return {
//       props: {
//         gameDetail,
//       },
//     };
//   }
// );

export default GameLain;
