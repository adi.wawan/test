import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { connect } from 'react-redux';
// import { auth, signinWithGoogle } from '@helpers/firebase';

import { authenticate } from '@redux/actions/auth.action';
import FormFragment from '../../fragment/form';

const Signin = ({ authenticate, state }) => {
  const auth = useSelector((state) => state.auth); // this is hook for accessing global state from redux

  const [data, setData] = useState({
    email: '',
    password: '',
  });
  const [error, setError] = useState(null);
  const [user, setUser] = useState(null);
  const [isSubmit, setIsSubmit] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setData({ ...data, [name]: value });
  };

  const handleSignin = async (e) => {
    e.preventDefault();
    authenticate(data);
  };

  // const handleGoogle = () => {
  //   setIsSubmit(true);
  //   signinWithGoogle();
  // };

  return (
    <>
      <FormFragment
        type="signin"
        handleChange={handleChange}
        handleSubmit={handleSignin}
        // handleGoogle={handleGoogle}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  return { state: state.auth };
};

const mapDispatchToProps = {
  authenticate,
};

export default connect(mapStateToProps, mapDispatchToProps)(Signin);
