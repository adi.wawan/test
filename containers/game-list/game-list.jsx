import React, { useEffect } from 'react';
import { Container, Card } from 'react-bootstrap';
import { withRouter } from 'next/router';
import { useSelector } from 'react-redux';
import Divider from '@components/divider';

const GameList = ({ gameList, router }) => {
  const { user } = useSelector((state) => state.auth);
  const { category } = useSelector((state) => state.cat);

  const check = (n, m) => {
    if (m.length > 0) {
      const filtered = m.map((x) => x.id);
      if (filtered.indexOf(n) !== -1) {
        return true;
      }
    }
    return false;
  };

  return (
    <div className="mt-5">
      <div className="">
        <h1 className="title mt-5">{category}</h1>
      </div>
      <div className="preview">
        <div className="card-deck row">
          {gameList?.map((item) =>
            category === 'all' ? (
              <div
                className={`col-lg-3 col-md-4 game-card-list box mb-2 pb-0 ${
                  check(user?.id, item.users) === true ? 'available' : ''
                }`}
                key={item.id}
                onClick={() => router.push(`games/detail/${item.id}`)}
              >
                <img
                  className="card-img-top img-fluid rounded"
                  src={item.thumbnail_url}
                  alt={item.name}
                />
                <div className="card-body" style={{ padding: 0 }}>
                  <h4
                    className="card-title"
                    style={{ fontSize: '12px', textAlign: 'center' }}
                  >
                    {item.title}
                  </h4>
                </div>
              </div>
            ) : (
              item.category.includes(category) && (
                <div
                  mb={4}
                  className={`col-lg-3 col-md-4 game-card-list box mb-2 pb-0 ${
                    check(user?.id, item.users) === true ? 'available' : ''
                  }`}
                  key={item.id}
                  onClick={() => router.push(`games/detail/${item.id}`)}
                >
                  <img
                    className="card-img-top img-fluid rounded"
                    src={item.thumbnail_url}
                    alt={item.name}
                  />
                  <div className="card-body" style={{ padding: 0 }}>
                    <h4
                      className="card-title"
                      style={{ fontSize: '12px', textAlign: 'center' }}
                    >
                      {item.title}
                    </h4>
                  </div>
                </div>
              )
            )
          )}
        </div>
      </div>
    </div>
  );
};

export default withRouter(GameList);
