import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { connect } from 'react-redux';

import { register } from '../../redux/actions/auth.action';
import FormFragment from '../../fragment/form';

const Signup = ({ register, auth }) => {
  // const auth = useSelector((state) => state.auth); // this is hook for accessing global state from redux

  const [data, setData] = useState({
    username: '',
    email: '',
    password: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setData({ ...data, [name]: value });
  };

  const handleSignup = async (e) => {
    e.preventDefault();
    register(data);
  };

  return (
    <>
      <FormFragment
        type="signup"
        handleChange={handleChange}
        handleSubmit={handleSignup}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  return { auth: state.auth };
};

const mapDispatchToProps = {
  register,
};

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
