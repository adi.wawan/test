export const RPS_MAP = [
  { name: 'batu', val: 1 },
  { name: 'kertas', val: 2 },
  { name: 'gunting', val: 3 },
];

const rules = {
  batu: 'gunting',
  gunting: 'kertas',
  kertas: 'batu',
};

export const getRandomComp = () => {
  return RPS_MAP[Math.floor(RPS_MAP.length * Math.random())]['name'];
};

export const getWinner = (p, c) => {
  if (p === c) return 'DRAW';
  if (c === rules[p]) return 'WINS';
  else return 'LOSES';
};
