/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useState, useEffect } from 'react';
import Link from 'next/link';
import { Container, Row, Col, Card } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import router from 'next/router';
import withAuth from '@helpers/withAuth';

import { callApi } from '@helpers/network';
import GamePayload from '@libraries/api/game';
import { toast } from 'react-toastify';
import axios from 'axios';

import { LOADING, STOPLOADING } from '@redux/types';
import { getCookie } from '@utils/cookies';
import { RPS_MAP, getRandomComp, getWinner } from './game.utils';

const GamePage = ({ gameId }) => {
  const { isLoading } = useSelector((state) => state.load);
  const dispatch = useDispatch();

  const [player, setPlayer] = useState(null);
  const [playerChoice, setPlayerChoice] = useState('');
  const [comChoice, setComChoice] = useState('');
  const [status, setStatus] = useState('');
  const [change, setChange] = useState(false);
  const [run, setIsRun] = useState(false);
  const [score, setScore] = useState({
    pScore: 0,
    cScore: 0,
  });

  const [playCount, setPlayCount] = useState(3);
  const [cnt, setCnt] = useState(0);

  // const [isLoading, setIsLoading] = useState(false);
  const [gameDetail, setGameDetail] = useState([]);

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (!token) {
      router.push(`/signin`);
    }
  }, []);

  const handleClick = (e) => {
    if (playCount > 0) {
      setCnt(cnt + 1);
      setPlayCount(playCount - 1);
      setIsRun(true);
      setPlayerChoice(e.target.id);
      const computerChoice = getRandomComp();
      setComChoice(computerChoice);
    }
  };

  const scoring = (s) => {
    switch (s) {
      case 'WINS':
        setScore({ ...score, pScore: score.pScore + 1 });
        break;
      case 'LOSES':
        setScore({ ...score, cScore: score.cScore + 1 });
        break;
      case 'DRAW':
        setScore({ ...score });
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    if (playerChoice && playCount >= 0) {
      const res = getWinner(playerChoice, comChoice);
      scoring(res);
      setStatus(res);
      setChange(true);
    }
  }, [comChoice, playerChoice, run]);

  useEffect(() => {
    setTimeout(() => {
      setChange(false);
    }, 1500);
  }, [change]);

  useEffect(() => {
    const getplayer = localStorage.getItem('user');
    setPlayer(JSON.parse(getplayer));
  }, []);

  useEffect(() => {
    if (run) {
      setGameDetail([
        ...gameDetail,
        {
          round: cnt,
          score: `${score.pScore} - ${score.cScore}`,
          status: status,
        },
      ]);
    }
  }, [score]);

  // menang +3, seri +1, lose 0
  const point = (p, c) => {
    if (p > c) {
      return 3;
    } else if (p < c) {
      return 0;
    } else {
      return 1;
    }
  };

  const resultStatus = (p, c) => {
    if (p > c) {
      return 'win';
    } else if (p < c) {
      return 'lose';
    } else {
      return 'draw';
    }
  };

  const configToken = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `${getCookie('token')}`,
    },
  };

  // store data ke BE
  const dataScores = {
    game_score: `${score.pScore} : ${score.cScore}`,
    user_point: point(score.pScore, score.cScore),
    status: resultStatus(score.pScore, score.cScore),
  };

  const handleSubmit = async (e) => {
    dispatch({ type: LOADING });
    // alert(JSON.stringify(dataScores));
    // setIsLoading(true);
    await axios
      .put(
        `${process.env.API_HOST}/v2/games/${gameId}`,
        dataScores,
        configToken
      )
      .then((res) => {
        dispatch({ type: STOPLOADING });
        toast.success('Success..');
        handleRefres();
      })
      .catch((err) => {
        dispatch({ type: STOPLOADING });
        toast.error(err);
      });
    // const payload = GamePayload.updateScore(gameId, dataScores);
    // const result = await callApi(payload);
    // if (result) {
    //   setIsLoading(false);
    //   toast.success('Success!');
    // }
    // setIsLoading(false);
  };

  const handleChange = (e) => {};

  const handleRefres = (e) => {
    e.preventDefault();
    setIsRun(false);
    setPlayerChoice('');
    setComChoice('');
    setScore({
      pScore: 0,
      cScore: 0,
    });
    setChange(false);
    setStatus('');
    setPlayCount(3);
    setGameDetail([]);
    setCnt(0);
  };

  return (
    <>
      <Card
        className="p-3 bg-dark pt-5"
        style={{ maxWidth: '100vw', backgroundColor: '##4C9654' }}
      >
        <div className="arena">
          <Row>
            <Col sm={4} className="player">
              <p className="name player-name text-center">{player?.username}</p>
              {RPS_MAP.map((item) => (
                <div
                  key={item.val}
                  className={`col mx-auto ${item.name} ${
                    item.name === playerChoice ? 'isactive' : 'inactive'
                  }`}
                  id={item.name}
                  style={{
                    cursor: `${playCount <= 0 ? 'not-allowed' : 'pointer'}`,
                  }}
                  onClick={(e) => handleClick(e)}
                  onKeyDown={(e) => handleClick(e)}
                />
              ))}
            </Col>
            <Col
              sm={4}
              className="mx-auto d-flex flex-column justify-content-between v"
              id="versus"
            >
              <Col className="d-flex justify-content-between score">
                <div>
                  <h1 id="player-score">{score.pScore}</h1>
                </div>
                <div>
                  <h1 id="comp-score">{score.cScore}</h1>
                </div>
              </Col>
              <Col className="mx-auto">
                <p
                  className={`text-center mx-auto info vs ${
                    !change ? '' : ''
                  } ${playCount <= 0 ? 'disable' : ''}`}
                >
                  {status ? status : 'VS'}
                </p>
              </Col>
            </Col>

            <Col sm={4} className="comp" id="comp">
              <p className="name text-center">Comp</p>
              {RPS_MAP.map((item) => (
                <div
                  key={item.val}
                  className={`col mx-auto ${item.name} ${
                    item.name === comChoice ? 'isactive' : 'inactive'
                  }`}
                  id={item.name}
                />
              ))}
            </Col>
          </Row>
          <Row className="d-flex justify-content-center">
            <div
              className="refresh"
              id="refresh"
              onClick={(e) => handleRefres(e)}
            ></div>
            <button
              type="button"
              className="btn btn-success"
              disabled={playCount > 0 ? true : false}
              onClick={handleSubmit}
            >
              {isLoading ? 'Procesing..' : 'Simpan Hasil'}
            </button>
          </Row>
        </div>
      </Card>
      <div className="list-game-detail">
        {gameDetail.map((item, i) => (
          <div
            className="d-flex justify-content-center"
            // style={{ visibility: `${i === 0 ? 'hidden' : ''}` }}
          >
            <h4 className="mr-3">Round: {item.round}</h4>
            <h4 className="mr-3">Score: {item.score}</h4>
            <h4 className="mr-3">{item.status}</h4>
          </div>
        ))}
        {playCount === 0 && (
          <h2 className="text-center">
            {resultStatus(score.pScore, score.cScore)}
          </h2>
        )}
      </div>
    </>
  );
};

export default GamePage;
